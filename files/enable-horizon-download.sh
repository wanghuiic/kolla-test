#!/bin/bash

# container
coname="horizon"
# web directory
srcdir="/tmp/docroot"
dstdir="/var/www/html/files"

t=$(docker ps -f "status=running" -f "name=$coname" --format "{{.Names}}")
[ -n "$t" ] || { echo "$coname container not running!" >&2; exit 1; }

# container OS
unset package

docker exec $coname apt show apache2 &>/dev/null
if [ $? -eq 0 ]; then
    package="apache2"
    httpconf="/etc/apache2/apache2.conf"
else
    docker exec -it horizon rpm -q httpd &>/dev/null
    if [ $? -eq 0 ]; then
        package="httpd"
        httpconf="/etc/httpd/conf/httpd.conf"
    fi
fi
[ -n "$package" ] || exit 1

set -x

mkdir -p $srcdir
chmod -R 755 $srcdir

docker exec $coname sh -c "[ -h $dstdir ] || ln -s $srcdir $dstdir"
if [ "$package" == "httpd" ]; then
    docker exec $coname sed -i \
     -e '/^[[:blank:]]*Alias \/files '${dstdir//\//\\/}'/d' \
     -e '/^<IfModule alias_module>.*/a Alias \/files '${dstdir//\//\\/} \
     $httpconf
else
    docker cp apache2.conf $coname:$httpconf
fi
echo docker restart $coname
docker restart $coname
