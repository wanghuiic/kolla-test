#!/bin/bash

get_active_compute_nodes() {
    local node=
    IFS=$'\n'
    for x in $(docker exec -it kolla_toolbox \
     /bin/bash -c ". /admin-openrc.sh && \
     openstack compute service list --service nova-compute \
     -c Host -c State -f value|grep up"); do
        unset IFS
        read node tmp <<< $x
        echo -n "$node "
    done
}

compute_nodes="$(get_active_compute_nodes)"

for x in $compute_nodes;do
    sudo scp config.json polling.yaml root@$x:/etc/kolla/ceilometer-compute/
    sudo ssh root@$x docker restart ceilometer_compute
done

