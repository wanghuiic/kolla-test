#!/bin/bash

coname="ceilometer_notification"
t=$(docker ps -f "status=running" -f "name=$coname" --format "{{.Names}}")
[ "$t" == "$coname" ] || { echo "$coname container not running!" >&2; exit 1; }

pyver=$(docker exec $coname /bin/bash -c "echo \$KOLLA_DISTRO_PYTHON_VERSION")
CEILOMETER_PATH="/var/lib/kolla/venv/lib/python${pyver}/site-packages/ceilometer"

docker cp gnocchi_resources.yaml \
 ${coname}:${CEILOMETER_PATH}/publisher/data/gnocchi_resources.yaml && \
  docker restart $coname || exit -2

