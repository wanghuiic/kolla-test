#!/bin/bash

show_help_install() {
        echo -e "install TianNi Openstack.\nUsage:\n\t$myname [options]\nOptions:"
        echo -e "\t--help, -h              \tshow help."
        echo -e "\t--skip-bootstrap        \tskip kolla bootstrap."
        echo -e "\t--skip-pre              \tskip kolla prechecks."
        echo -e "\t--skip-deploy           \tskip kolla deploy."
        echo -e "\t--skip-post             \tskip kolla post-deploy."
        echo -e "\t--skip-extra            \tskip tasks in extras.yml"
        echo -e "\t--skip-all              \tskip kolla bootstrap, prechecks, deploy, post-deploy and extra"
        echo -e "\t--skip-install-kolla    \tskip install kolla."
        echo -e "\t--select [nova|...]     \tselect nova|glance|cinder|neutron|swift|ceilometer|heat|prometheus"
        echo -e "\t                        \t       |ironic|manila|octavia|designate|trove|xtrabackup..."
        echo -e "\t                        \t(by adding '-e enable_xxx=no' options to kolla-ansible cmdline)"
        echo -e "\t--genpwd                \tcall kolla-genpwd."
        echo -e "\t--confdir, -d <dir>     \tdirectory of scratch passwords.yml, globals.yml and"
        echo -e "\t                        \tINVENTORY(e.g. multinode)"
        echo -e "Example:"
        echo -e "\t$myname --skip-install-kolla --confdir work/149_28 --skip-extra"
        echo -e "\t$myname --skip-install-kolla --select cinder --confdir work/149_28 --skip-extra -- --limit node30"
        echo -e "\t$myname --skip-install-kolla --confdir work/149_28 --skip-bootstrap --select prometheus"
        echo -e "\tinstall Zun:"
        echo -e "\t$myname --skip-install-kolla --confdir work/149_28 --select kuryr --select etcd --select zun --select horizon"
        echo -e "\t$myname --skip-install-kolla --confdir work/149_28 --select manila --select horizon"
        echo -e "\tAdd compute node:"
        echo -e "\t$myname --skip-install-kolla --confdir work/144_50 --skip-extra -- --limit gb21"
        echo -e "Note:"
        echo -e "\tcinder_target_helper is set to lioadm."
}

cmdopts=$(getopt --longoptions skip-bootstrap,skip-pre,skip-deploy,skip-post,skip-extra,skip-all,skip-install-kolla,genpwd,title:,select:,confdir:,help \
                     --options +ht:d: -- "$@")
if [ $? -ne 0 ] ; then
  echo "Terminating..." 1>&2
  exit 1
fi

# set positional parameters
eval set -- "$cmdopts"

skip_bootstrap=false
skip_pre=false
skip_deploy=false
skip_post=false
skip_extra=false
skip_install_kolla=false
myname=$0
gen_pwd=false
confdir=.
_comps=(
enable_ceilometer=no
enable_cloudkitty=no
enable_gnocchi=no
enable_grafana=no
enable_influxdb=no
enable_nova=no
enable_glance=no
enable_cinder=no
enable_swift=no
enable_heat=no
enable_horizon=no
enable_neutron=no
enable_keystone=no
## order is important -->
enable_xtrabackup=no
enable_mariadb=no
## <-- order is important
enable_rabbitmq=no
enable_memcached=no
enable_prometheus=no
enable_ironic=no
enable_manila=no
enable_zun=no
enable_kuryr=no
enable_etcd=no
enable_octavia=no
enable_designate=no
enable_trove=no
)
selectmode=false

select_comps() {
    # e.g. nova
    local o=$1
    local i=0
    while ((i<${#_comps[@]}));do
      if [ "${_comps[$i]}" == "enable_${o}=no" ]; then
          _comps[$i]="enable_${o}=yes"
          break
      fi
      ((i++))
    done

    i=0
    flag=false
    while ((i<${#_comps[@]}));do
      if $flag; then
          if [ "${_comps[$i]}" == "enable_mariadb=no" ]; then
              _comps[$i]="enable_mariadb=yes"
              break
          fi
      fi
      if [ "${_comps[$i]}" == "enable_xtrabackup=yes" ]; then
          flag=true
      fi
      ((i++))
    done
}

while true; do
  case "$1" in
    -h | --help )
        show_help_install
        exit ;;
   -t | --title )
        myname="$2"
        shift 2;;
   -d | --confdir )
        confdir="$2"
        shift 2;;
   --select )
        selectmode=true
        select_comps "$2"
        shift 2;;
    --skip-install-kolla )
        skip_install_kolla=true
        shift ;;
    --genpwd )
        gen_pwd=true
        shift ;;
    --skip-bootstrap )
        skip_bootstrap=true; shift ;;
    --skip-pre )
        skip_pre=true; shift ;;
    --skip-deploy )
        skip_deploy=true; shift ;;
    --skip-post )
        skip_post=true; shift ;;
    --skip-extra )
        skip_extra=true; shift ;;
    --skip-all )
        skip_bootstrap=true
        skip_pre=true
        skip_deploy=true
        skip_post=true
        skip_extra=true
        shift ;;
    -- ) shift; break ;;
    * ) break ;;
  esac
done

selectstr=

if $selectmode; then
  for x in ${_comps[@]}; do
      selectstr="$selectstr -e $x "
  done
fi

argstr1=
argstr2="$@ -e WORKDIR=$WORKDIR $selectstr"

if $skip_install_kolla; then
    argstr1="--skip-install-kolla "
fi

if $gen_pwd; then
    argstr1="$argstr1 --genpwd "
fi

. tn-openstack-common $argstr1 --confdir $confdir

if ! $skip_bootstrap; then
    kolla-ansible -i $ETCKOLLA/$INVENTORY $argstr2 bootstrap-servers || exit 1
fi

if ! $skip_pre; then
    kolla-ansible -i $ETCKOLLA/$INVENTORY $argstr2 \
      -e enable_cinder_backend_iscsi=yes \
      -e cinder_target_helper=tgtadm \
      prechecks || exit 2
fi

if ! $skip_deploy; then
    kolla-ansible -i $ETCKOLLA/$INVENTORY $argstr2 \
      -e enable_cinder_backend_iscsi=yes \
      -e cinder_target_helper=tgtadm \
      deploy || exit 3
fi

if ! $skip_post; then
    kolla-ansible -i $ETCKOLLA/$INVENTORY $argstr2 post-deploy || exit 4
fi

if ! $skip_extra; then
    echo -e "\nansible-playbook -e @$ETCKOLLA/globals.yml -e @$ETCKOLLA/extras.var -e cinder_target_helper=lioadm -i $ETCKOLLA/$INVENTORY $argstr2 extras.yml"
    ansible-playbook -e @$ETCKOLLA/globals.yml -e @$ETCKOLLA/extras.var -e cinder_target_helper=lioadm -i $ETCKOLLA/$INVENTORY $argstr2 extras.yml || exit 5
fi
