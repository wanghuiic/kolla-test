#!/bin/bash

show_help_mariadb() {
        echo -e "mariadb backup | recovery on TianNi Openstack.\nUsage:\n\t$Usage\nOptions:"
        echo -e "\t--skip-install-kolla    \tskip install kolla."
        echo -e "\t--action                \tbackup | recovery"
        echo -e "\t--confdir, -d <dir>     \tdirectory of scratch passwords.yml, globals.yml and"
        echo -e "kolla-ansible options for backup:"
        echo -e "\t        --full (default)"
        echo -e "\t        --incremental"
}

cmdopts=$(getopt --longoptions skip-install-kolla,title:,action:,confdir:,help \
                     --options +ht:d: -- "$@")
if [ $? -ne 0 ] ; then
  echo "Terminating..." 1>&2
  exit 1
fi

# set positional parameters
eval set -- "$cmdopts"

Usage="$0 [options...|--] [-- kolla-ansible_options]"
skip_install_kolla=false
action=
confdir=.

while true; do
  case "$1" in
    -h | --help )
        show_help_mariadb
        exit ;;
   --skip-install-kolla )
        skip_install_kolla=true
        shift ;;
   --action )
        action="$2"
        shift 2;;
   -t | --title )
        Usage="$2"
        shift 2;;
   -d | --confdir )
        confdir="$2"
        shift 2;;
    -- ) shift; break ;;
    * ) break ;;
  esac
done

if [ "$action" == "backup" ]; then
    :
elif  [ "$action" == "recovery" ]; then
    :
else
   show_help_mariadb
   exit 1 
fi

argstr1=

if $skip_install_kolla; then
    argstr1="--skip-install-kolla "
fi

. tn-openstack-common $argstr1 --confdir $confdir

kolla-ansible -i $ETCKOLLA/$INVENTORY -e enable_mariabackup=yes mariadb_$action $@
